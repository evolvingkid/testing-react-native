# name

Keep it brief and use correct terms

# Description

Give a breif description on how the bug occured.

# Environment

Tell us about what env used like qa, dev, prod

# device

Tell us about device used to check like web, android, ios

# logs

If their is any logs paste it.

# Steps to reproduce

steps to reproduce the issues

# Expected

Please explain the expected output

# Actual

Please explain the actual output
